import * as React from "react"
import { graphql, Link } from "gatsby"
import styled from 'styled-components'
import Layout from "../components/layout"
import Seo from "../components/seo"

const BlogLink = styled(Link)`
text-decoration: none;
`;

const BlogTitle = styled.h3`
margin-bottom: 20px;
color: blue;
`;




export default ({data}) => (
  <Layout>
    <SEO title="Blog" />
    <div>
      <h1>My h1</h1>
      <h4>{ data.allMarkdownRemark.totalCount}</h4>
      {
        data.allMarkdownRemark.edges.map(({node}) => (
          <div key={node.id}>
            <BlogLink to={node.fields.slug}>
              <BlogTitle>
                { node.frontmatter.title } - { node.frontmatter.date } 
              </BlogTitle>
            </BlogLink>
            <p>{ node.excerpt }</p>
          </div>
        ))
      }
    </div>
    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export const Head = () => <Seo title="Page Three" />

export const query = graphql`
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            date
            description
            title
          }
          fields {
            slug
          }
          excerpt
          html
        }
      }
    }
  }
`;